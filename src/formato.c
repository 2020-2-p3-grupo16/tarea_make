#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void formato(char argf[]){

	char *fecha_nueva;
	char *hora_nueva;
	char delimiter[2][2] = {{" "},{"-"}};
	char *token = strtok(argf,delimiter[0]);
	int i = 0;

	while(token != NULL) {
		if(i == 0){
			fecha_nueva=token;
			i += 1;
		}else if(i == 1){
			hora_nueva=token;
			i += 1;
		}
		token = strtok(NULL,delimiter[0]);	
	}
	
	char *new_token = strtok(fecha_nueva , delimiter[1]);

	
	int mes;
	char *dia;
	char *anio;
	int j=0;

	while(new_token != NULL){
		if(j==0){
			anio = new_token;
                        j++;
		}else if(j==1){
			mes=atoi(new_token);
			j++;
		}else if(j==2){
			dia=new_token;
                        j++;
		}
		new_token=strtok(NULL,delimiter[1]);
	}
	
	char lista[12][12]={{"Enero"},{"Febrero"},{"Marzo"},{"Abril"},{"Mayo"},{"Junio"},{"Julio"},{"Agosto"},{"Septiembre"},{"Octubre"},{"Noviembre"},{"Diciembre"}};
      	
	printf("%s de %s de %s, %s \n",dia,lista[mes-1],anio,hora_nueva);
}
