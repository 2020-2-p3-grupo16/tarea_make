
bin/utilfecha: obj/main.o obj/segundos.o obj/dias.o obj/formato.o
	gcc $^ -o $@

obj/main.o: src/main.c
	gcc -Wall -c -I include/ $^ -o $@


obj/segundos.o: src/segundos.c
	gcc -c src/segundos.c -o obj/segundos.o

obj/dias.o: src/dias.c
	gcc -c src/dias.c -o obj/dias.o

obj/formato.o: src/formato.c
	gcc -c src/formato.c -o obj/formato.o

.PHONY: clean
clean:
	rm obj/* bin/*
